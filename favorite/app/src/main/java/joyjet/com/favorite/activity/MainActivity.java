package joyjet.com.favorite.activity;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import joyjet.com.favorite.R;
import joyjet.com.favorite.adapter.GaleriaAdapter;
import joyjet.com.favorite.app.FavoriteApp;
import joyjet.com.favorite.http.HttpRequest;
import joyjet.com.favorite.model.Categoria;
import joyjet.com.favorite.model.Galeria;
import joyjet.com.favorite.model.ItemCategoria;

public class MainActivity extends DrawerLayoutActivity {

    ScrollView scrollViewCategoria;
    LinearLayout linearLayoutListaCategoria;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        View view = LayoutInflater.from(this).inflate(R.layout.activity_main, null);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu));

        getSupportActionBar().setTitle("Digital Space");

        scrollViewCategoria = (ScrollView) view.findViewById(R.id.scroll_view_categoria);

        linearLayoutListaCategoria = (LinearLayout) view.findViewById(R.id.linear_layout_lista_categoria);

        if(FavoriteApp.getMapItensPorCategoria() != null &&
                FavoriteApp.getMapItensPorCategoria().size() > 0){
            for (String key : FavoriteApp.getKeys()){
                Categoria categoria = new Categoria();
                categoria.setNome(key);
                categoria.setItensCategoria(FavoriteApp.getMapItensPorCategoria().get(key));
                createViewCabecalho(categoria);
            }
        }else{
            new BuscarCategoriasAsync().execute();
        }

        contentFrame.addView(view);
    }

    /*@TODO Função que cria o cabeçalho das categorias*/
    public void createViewCabecalho(Categoria categoria){
        LinearLayout linearLayout = new LinearLayout(MainActivity.this);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearLayout.setLayoutParams(layoutParams);
        linearLayout.setGravity(Gravity.CENTER|Gravity.LEFT);
        linearLayout.setBackgroundColor(Color.parseColor("#538BC8"));

        TextView textView = new TextView(MainActivity.this);
        LinearLayout.LayoutParams layoutParamsTextView = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParamsTextView.setMargins(10, 10, 15, 15);
        textView.setLayoutParams(layoutParamsTextView);
        textView.setTextSize(16f);
        textView.setTextColor(Color.parseColor("#ffffff"));
        textView.setText(categoria.getNome());

        linearLayout.addView(textView);
        linearLayoutListaCategoria.addView(linearLayout);

        int indice = 0;
        for (ItemCategoria itemCategoria : categoria.getItensCategoria()){
            linearLayoutListaCategoria.addView(createViewListagem(indice, categoria.getNome(), itemCategoria.getTitulo(), itemCategoria.getDescricao(), itemCategoria.getGaleriaList()));
            indice++;
        }
    }

    /*@TODO Função que cria a listagem dos itens*/
    public View createViewListagem(final int indiceItem, final String tituloCategoria, final String tituloItem, String descricao, final List<Galeria> galerias){
        View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.listagem_categoria, null);

        TextView textViewTituloItem = (TextView)view.findViewById(R.id.text_view_titulo_item);
        TextView textViewDescricao = (TextView)view.findViewById(R.id.text_view_descricao);

        final ViewPager viewPagerGaleria = (ViewPager) view.findViewById(R.id.view_pager_galeria);

        LinearLayout imageViewSetaEsquerda = (LinearLayout) view.findViewById(R.id.image_view_seta_esquerda);
        LinearLayout imageViewSetaDireita = (LinearLayout) view.findViewById(R.id.image_view_seta_direita);

        final int[] indicePager = {0};
        imageViewSetaDireita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicePager[0]++;
                if(indicePager[0] > galerias.size() - 1){
                    indicePager[0] = galerias.size() - 1;
                }

                viewPagerGaleria.setCurrentItem(indicePager[0]);
            }
        });

        imageViewSetaEsquerda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicePager[0]--;
                if(indicePager[0] < 0){
                    indicePager[0] = 0;
                }

                viewPagerGaleria.setCurrentItem(indicePager[0]);
            }
        });

        viewPagerGaleria.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                indicePager[0] = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        LinearLayout linearLayoutProgressBarImagens = (LinearLayout) view.findViewById(R.id.linear_layout_progress_bar_imagens);

        if(FavoriteApp.getMapImagensPorItem().size() > 0 &&
                FavoriteApp.getMapImagensPorItem().get(tituloCategoria + "_" + indiceItem) != null){
            linearLayoutProgressBarImagens.setVisibility(View.GONE);
            GaleriaAdapter galeriaAdapter = new GaleriaAdapter(MainActivity.this,
                    FavoriteApp.getMapImagensPorItem().get(tituloCategoria + "_" + indiceItem),
                    tituloCategoria,
                    indiceItem, true, true);
            viewPagerGaleria.setAdapter(galeriaAdapter);
            galeriaAdapter.notifyDataSetChanged();
        }else{
            new BuscaImagemAsync(viewPagerGaleria, galerias, linearLayoutProgressBarImagens, tituloCategoria, indiceItem).execute();
        }

        //new BuscaImagemAsync(viewPagerGaleria, galerias, progressBarImagens, tituloCategoria, indiceItem).execute();

        textViewTituloItem.setText(tituloItem);
        textViewDescricao.setText(descricao.substring(0, 56));

        return view;
    }

    /*@TODO Classe busca as categorias*/
    class BuscarCategoriasAsync extends AsyncTask<Void, Void, JSONArray>{
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setTitle("Aguarde...");
            progressDialog.setMessage("Buscando categorias");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONArray doInBackground(Void... params) {
            JSONArray result = null;

            try{
                result = HttpRequest.getJSONObjectFromURL("https://cdn.joyjet.com/tech-interview/mobile-test-one.json");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return result;
        }

        @Override
        protected void onPostExecute(JSONArray result) {
            super.onPostExecute(result);

            progressDialog.dismiss();
            Map<String, List<ItemCategoria>> mapItensPorCategoria = new HashMap<String, List<ItemCategoria>>();
            List<ItemCategoria> itens = new ArrayList<ItemCategoria>();
            if(result != null){
                int countArray = result.length();
                try{
                    for (int i = 0;i < countArray;i++){
                        JSONObject object = (JSONObject) result.get(i);
                        FavoriteApp.getKeys().add(object.getString("category"));
                        if(mapItensPorCategoria.containsKey(object.getString("category"))){
                            itens = mapItensPorCategoria.get(object.getString("category"));
                        }else{
                            itens = new ArrayList<ItemCategoria>();
                        }

                        JSONArray itensCategoriaArray = object.getJSONArray("items");
                        int countItens = itensCategoriaArray.length();
                        for (int j = 0;j < countItens;j++){
                            JSONObject objectItem = (JSONObject) itensCategoriaArray.get(j);
                            ItemCategoria itemCategoria = new ItemCategoria();
                            itemCategoria.setTitulo(objectItem.getString("title"));
                            itemCategoria.setDescricao(objectItem.getString("description"));
                            JSONArray galeriaArray = objectItem.getJSONArray("galery");
                            List<Galeria> galerias = new ArrayList<Galeria>();
                            for(int m = 0;m < galeriaArray.length();m++){
                                Galeria galeria = new Galeria();
                                galeria.setCaminho(galeriaArray.getString(m));
                                galerias.add(galeria);
                            }

                            itemCategoria.setGaleriaList(galerias);

                            itens.add(itemCategoria);
                        }

                        mapItensPorCategoria.put(object.getString("category"), itens);
                    }

                    FavoriteApp.setMapItensPorCategoria(mapItensPorCategoria);

                    for (String key : FavoriteApp.getKeys()){
                        Categoria categoria = new Categoria();
                        categoria.setNome(key);
                        categoria.setItensCategoria(mapItensPorCategoria.get(key));
                        createViewCabecalho(categoria);
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(MainActivity.this, "Não possivel carregar as categorias", Toast.LENGTH_LONG).show();
            }
        }
    }

    /*@TODO Classe busca as imagens da galeria dos itens*/
    class BuscaImagemAsync extends AsyncTask<Void, Void, List<Bitmap>> {

        ViewPager viewPager;
        List<Galeria> galerias;
        LinearLayout linearLayoutProgressBarImagens;
        String categoria;
        int indiceItem;

        public BuscaImagemAsync(ViewPager viewPager, List<Galeria> galerias,
                                LinearLayout linearLayoutProgressBarImagens,
                                String categoria, int indiceItem) {
            this.viewPager = viewPager;
            this.galerias = galerias;
            this.linearLayoutProgressBarImagens = linearLayoutProgressBarImagens;
            this.categoria = categoria;
            this.indiceItem = indiceItem;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<Bitmap> result = new ArrayList<Bitmap>();

            try {
                for (Galeria galeria : galerias) {
                    HttpURLConnection connection = (HttpURLConnection) new URL(galeria.getCaminho()).openConnection();
                    connection.setRequestProperty("User-agent", "Mozilla/4.0");

                    connection.connect();
                    InputStream input = connection.getInputStream();

                    result.add(BitmapFactory.decodeStream(input));
                }

            } catch (Exception e) {
                e.printStackTrace();
                result = new ArrayList<Bitmap>();
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<Bitmap> result) {
            super.onPostExecute(result);
            if(result.size() > 0){
                linearLayoutProgressBarImagens.setVisibility(View.GONE);
                FavoriteApp.getMapImagensPorItem().put(categoria + "_" + indiceItem, result);
                GaleriaAdapter galeriaAdapter = new GaleriaAdapter(MainActivity.this, result, categoria, indiceItem, true, false);
                viewPager.setAdapter(galeriaAdapter);
                galeriaAdapter.notifyDataSetChanged();
            }else{
                linearLayoutProgressBarImagens.setVisibility(View.GONE);
            }
        }
    }
}
