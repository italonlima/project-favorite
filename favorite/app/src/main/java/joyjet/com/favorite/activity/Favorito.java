package joyjet.com.favorite.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import joyjet.com.favorite.R;
import joyjet.com.favorite.adapter.GaleriaAdapter;
import joyjet.com.favorite.app.FavoriteApp;
import joyjet.com.favorite.model.Galeria;
import joyjet.com.favorite.model.ItemCategoria;

public class Favorito extends DrawerLayoutActivity {

    ScrollView scrollViewFavorito;
    LinearLayout linearLayoutListaFavorito;
    Set<String> favoritos;
    Map<String, List<ItemCategoria>> mapItensPorCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_favorito);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu));

        getSupportActionBar().setTitle("Favorites");

        View view = LayoutInflater.from(this).inflate(R.layout.activity_favorito, null);

        SharedPreferences sharedPref = FavoriteApp.getPreferences(this);//getSharedPreferences("favoritos", Context.MODE_PRIVATE);
        favoritos = sharedPref.getStringSet("favoritosList", null);

        mapItensPorCategoria = FavoriteApp.getMapItensPorCategoria();

        scrollViewFavorito = (ScrollView) view.findViewById(R.id.scroll_view_favorito);

        linearLayoutListaFavorito = (LinearLayout) view.findViewById(R.id.linear_layout_lista_favorito);

        contentFrame.addView(view);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (favoritos != null){
            for (String favorito : favoritos){
                String[] indices = favorito.split("_");
                ItemCategoria itemCategoria = mapItensPorCategoria.get(indices[0]).get(Integer.parseInt(indices[1]));
                linearLayoutListaFavorito.addView(createViewListagem(Integer.parseInt(indices[1]), indices[0], itemCategoria.getTitulo(), itemCategoria.getDescricao(), itemCategoria.getGaleriaList()));
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    /*@TODO Função que cria a listagem dos itens*/
    public View createViewListagem(final int indiceItem, final String tituloCategoria, final String tituloItem, String descricao, final List<Galeria> galerias){
        View view = LayoutInflater.from(Favorito.this).inflate(R.layout.listagem_categoria, null);

        TextView textViewTituloItem = (TextView)view.findViewById(R.id.text_view_titulo_item);
        TextView textViewDescricao = (TextView)view.findViewById(R.id.text_view_descricao);

        LinearLayout imageViewSetaEsquerda = (LinearLayout) view.findViewById(R.id.image_view_seta_esquerda);
        LinearLayout imageViewSetaDireita = (LinearLayout) view.findViewById(R.id.image_view_seta_direita);

        final ViewPager viewPagerGaleria = (ViewPager) view.findViewById(R.id.view_pager_galeria);

        final int[] indicePager = {0};
        imageViewSetaDireita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicePager[0]++;
                if(indicePager[0] > galerias.size() - 1){
                    indicePager[0] = galerias.size() - 1;
                }

                viewPagerGaleria.setCurrentItem(indicePager[0]);
            }
        });

        imageViewSetaEsquerda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                indicePager[0]--;
                if(indicePager[0] < 0){
                    indicePager[0] = 0;
                }

                viewPagerGaleria.setCurrentItem(indicePager[0]);
            }
        });

        viewPagerGaleria.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                indicePager[0] = position;
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ProgressBar progressBarImagens = (ProgressBar) view.findViewById(R.id.progress_bar_imagens);

        if(FavoriteApp.getMapImagensPorItem().size() > 0 &&
                FavoriteApp.getMapImagensPorItem().get(tituloCategoria + "_" + indiceItem) != null){
            progressBarImagens.setVisibility(View.GONE);
            GaleriaAdapter galeriaAdapter = new GaleriaAdapter(Favorito.this,
                                                                FavoriteApp.getMapImagensPorItem().get(tituloCategoria + "_" + indiceItem),
                                                                tituloCategoria,
                                                                indiceItem, true, true);
            viewPagerGaleria.setAdapter(galeriaAdapter);
            galeriaAdapter.notifyDataSetChanged();
        }else{
            new BuscaImagemAsync(viewPagerGaleria, galerias, progressBarImagens, tituloCategoria, indiceItem).execute();
        }


        textViewTituloItem.setText(tituloItem);
        textViewDescricao.setText(descricao.substring(0, 56));

        return view;
    }

    /*@TODO Classe busca as imagens da galeria dos itens*/
    class BuscaImagemAsync extends AsyncTask<Void, Void, List<Bitmap>> {

        ViewPager viewPager;
        List<Galeria> galerias;
        ProgressBar progressBarImagens;
        String categoria;
        int indiceItem;

        public BuscaImagemAsync(ViewPager viewPager, List<Galeria> galerias,
                                ProgressBar progressBarImagens,
                                String categoria, int indiceItem) {
            this.viewPager = viewPager;
            this.galerias = galerias;
            this.progressBarImagens = progressBarImagens;
            this.categoria = categoria;
            this.indiceItem = indiceItem;
        }

        @Override
        protected List<Bitmap> doInBackground(Void... params) {
            List<Bitmap> result = new ArrayList<Bitmap>();

            try {
                for (Galeria galeria : galerias) {
                    HttpURLConnection connection = (HttpURLConnection) new URL(galeria.getCaminho()).openConnection();
                    connection.setRequestProperty("User-agent", "Mozilla/4.0");

                    connection.connect();
                    InputStream input = connection.getInputStream();

                    result.add(BitmapFactory.decodeStream(input));
                }

            } catch (Exception e) {
                e.printStackTrace();
                result = new ArrayList<Bitmap>();
            }
            return result;
        }

        @Override
        protected void onPostExecute(List<Bitmap> result) {
            super.onPostExecute(result);
            if(result.size() > 0){
                progressBarImagens.setVisibility(View.GONE);
                Map<String, List<Bitmap>> mapImagensPorItem = new HashMap<String, List<Bitmap>>();
                mapImagensPorItem.put(categoria + "_" + indiceItem, result);
                FavoriteApp.setMapImagensPorItem(mapImagensPorItem);
                GaleriaAdapter galeriaAdapter = new GaleriaAdapter(Favorito.this, result, categoria, indiceItem, true, true);
                viewPager.setAdapter(galeriaAdapter);
                galeriaAdapter.notifyDataSetChanged();
            }else{
                progressBarImagens.setVisibility(View.GONE);
            }
        }
    }
}
