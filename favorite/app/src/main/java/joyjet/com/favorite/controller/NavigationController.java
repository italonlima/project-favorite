package joyjet.com.favorite.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.support.v7.app.ActionBarActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import joyjet.com.favorite.R;
import joyjet.com.favorite.activity.DrawerLayoutActivity;
import joyjet.com.favorite.activity.Favorito;
import joyjet.com.favorite.activity.MainActivity;
import joyjet.com.favorite.model.SlideMenuItem;

public class NavigationController {

    private ActionBarActivity actionBarActivity;

    public static final Integer CODIGO_ACAO_HOME = 1;
    public static final Integer CODIGO_ACAO_FAVORITE = 2;

    private static NavigationController instance;
    private Map<Integer, List<SlideMenuItem>> itensDosMenus = new HashMap<Integer, List<SlideMenuItem>>();

    private Integer acaoFragmentoAtual;

    public NavigationController(ActionBarActivity actionBarActivity){
        this.actionBarActivity = actionBarActivity;
    }

    public void executarAcao(int codigoAcao, DrawerLayoutActivity activity){
        if (codigoAcao == CODIGO_ACAO_HOME)
        {
            activity.startActivity(new Intent(activity, MainActivity.class));
            activity.finish();
        }
        else if (codigoAcao == CODIGO_ACAO_FAVORITE){
            activity.startActivity(new Intent(activity, Favorito.class));
        }
    }

    @SuppressWarnings("ResourceType")
    private List<SlideMenuItem> getItensMenu(int idListaMenus) throws Exception {
        Resources res = actionBarActivity.getResources();
        TypedArray ta = res.obtainTypedArray(idListaMenus);
        List<SlideMenuItem> retorno = new ArrayList<SlideMenuItem>();
        for (int i = 0; i < ta.length(); ++i) {
            int idMenu = ta.getResourceId(i, 0);
            if (idMenu > 0) {
                TypedArray taMenu = res.obtainTypedArray(idMenu);
                if(taMenu.length() < 2){
                    throw new Exception("O menu não possui o formato correto no arquivo arrays.xml" + i);
                }
                String titulo = taMenu.getString(0);
                int idAcao = taMenu.getInt(1,0);

                retorno.add(new SlideMenuItem(titulo, idAcao));
            } else {
                throw new Exception("O id do menu informado não existe na configuração de menus: " + i );
            }
        }
        ta.recycle();
        itensDosMenus.put(idListaMenus, retorno);
        return retorno;
    }

    public List<SlideMenuItem> getItensMenu() throws Exception {
        return getItensMenu(R.array.itens_menu);
    }
}
