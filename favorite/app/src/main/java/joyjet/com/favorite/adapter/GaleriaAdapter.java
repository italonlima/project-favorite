package joyjet.com.favorite.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import joyjet.com.favorite.R;
import joyjet.com.favorite.activity.DetalheCategoria;
import joyjet.com.favorite.activity.DrawerLayoutActivity;
import joyjet.com.favorite.activity.Favorito;
import joyjet.com.favorite.activity.MainActivity;
import joyjet.com.favorite.app.FavoriteApp;
import joyjet.com.favorite.model.Galeria;

public class GaleriaAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;
    List<Bitmap> imagensGaleria;
    String categoria;
    int indiceItem;
    boolean isClickable = true, isRemoveFavorito = false;

    public GaleriaAdapter(Context context, List<Bitmap> imagensGaleria, String categoria,
                          int indiceItem, boolean isClickable, boolean isRemoveFavorito) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.imagensGaleria = imagensGaleria;
        this.categoria = categoria;
        this.indiceItem = indiceItem;
        this.isClickable = isClickable;
        this.isRemoveFavorito = isRemoveFavorito;
    }

    @Override
    public int getCount() {
        return imagensGaleria.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = layoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.image_view_galeria);
        if(imagensGaleria.get(position) != null){
            imageView.setBackground(new BitmapDrawable(imagensGaleria.get(position)));
        }

        if (isClickable){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetalheCategoria.class);
                    intent.putExtra("categoria", categoria);
                    intent.putExtra("itemIndice", indiceItem);
                    context.startActivity(intent);
                }
            });
        }

        if(isRemoveFavorito){
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    SharedPreferences sharedPref = FavoriteApp.getPreferences((DrawerLayoutActivity) context);//getSharedPreferences("favoritos", Context.MODE_PRIVATE);
                    Set<String> favoritos = sharedPref.getStringSet("favoritosList", null);
                    favoritos.remove(categoria + "_" + indiceItem);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putStringSet("favoritosList", favoritos);
                    editor.apply();
                    Toast.makeText(context, "Favorito removido com sucesso", Toast.LENGTH_LONG).show();
                    ((DrawerLayoutActivity) context).finish();
                    return false;
                }
            });
        }

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
