package joyjet.com.favorite.activity;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ListView;

import java.util.List;

import joyjet.com.favorite.R;
import joyjet.com.favorite.adapter.MenuAdapter;
import joyjet.com.favorite.controller.NavigationController;
import joyjet.com.favorite.model.SlideMenuItem;
import joyjet.com.favorite.widget.ActionBarDrawableToogle;

public class DrawerLayoutActivity extends ActionBarActivity {
    DrawerLayout drawerLayout;
    private ListView drawerList;
    private ActionBarDrawableToogle drawerToggle;
    Toolbar toolbar;
    ViewGroup drawerContent;
    NavigationController navigation;
    private List<SlideMenuItem> menuItems;
    private MenuAdapter menuAdapter;
    private ListView listViewMenu;
    FrameLayout contentFrame;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_layout);

        getWindow().setStatusBarColor(Color.parseColor("#20000000"));

        navigation = new NavigationController(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.parseColor("#5AA3E8"));
        setSupportActionBar(toolbar);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layoutt);
        drawerList = (ListView) findViewById(R.id.left_drawer);
        drawerContent = (ViewGroup) findViewById(R.id.drawer_content);

        listViewMenu = (ListView) findViewById(R.id.left_drawer);

        contentFrame = (FrameLayout) findViewById(R.id.content_frame);

        drawerToggle = new ActionBarDrawableToogle(this,
                drawerLayout,
                R.string.openDrawer,
                R.string.closeDrawer
        );
        drawerToggle.setDrawerIndicatorEnabled(false);
        drawerLayout.setDrawerListener(drawerToggle);
        drawerToggle.syncState();

        try {
            menuItems = navigation.getItensMenu();
            menuAdapter = new MenuAdapter(this, menuItems, navigation);
            listViewMenu.setAdapter(menuAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT))
        {
            outState.putString("DrawerState", "Opened");
        }

        else
        {
            outState.putString("DrawerState", "Closed");
        }

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                drawerLayout.openDrawer(drawerContent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
