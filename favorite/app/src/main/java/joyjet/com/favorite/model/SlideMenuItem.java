package joyjet.com.favorite.model;

public class SlideMenuItem {
    private String name;
    private Integer codigoAcao;

    public SlideMenuItem(String name, Integer codigoAcao) {
        this.name = name;
        this.codigoAcao = codigoAcao;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCodigoAcao() {
        return codigoAcao;
    }

    public void setCodigoAcao(Integer codigoAcao) {
        this.codigoAcao = codigoAcao;
    }
}
