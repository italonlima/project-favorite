package joyjet.com.favorite.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import joyjet.com.favorite.R;
import joyjet.com.favorite.activity.DrawerLayoutActivity;
import joyjet.com.favorite.controller.NavigationController;
import joyjet.com.favorite.model.SlideMenuItem;

public class MenuAdapter extends BaseAdapter {

    private List<SlideMenuItem> listaMenus;
    private Activity context;
    private NavigationController navigation;

    public MenuAdapter(Activity context,
                       List<SlideMenuItem> listaMenus,
                       NavigationController navigation){
        this.context = context;
        this.listaMenus = listaMenus;
        this.navigation = navigation;

    }

    @Override
    public int getCount() {
        return listaMenus.size();
    }

    @Override
    public Object getItem(int position) {
        return listaMenus.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.menu_adapter, null);

        final SlideMenuItem menuItem = listaMenus.get(position);

        TextView menuTitle = (TextView) view.findViewById(R.id.adapter_menu_name);
        menuTitle.setText(menuItem.getName());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigation.executarAcao(menuItem.getCodigoAcao(), (DrawerLayoutActivity) context);
            }
        });

        return view;
    }
}
