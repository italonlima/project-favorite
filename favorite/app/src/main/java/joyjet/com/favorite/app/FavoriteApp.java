package joyjet.com.favorite.app;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import joyjet.com.favorite.activity.DrawerLayoutActivity;
import joyjet.com.favorite.model.ItemCategoria;

public class FavoriteApp extends Application {
    private static Map<String, List<ItemCategoria>> mapItensPorCategoria;
    private static Map<String, List<Bitmap>> mapImagensPorItem;
    private static List<String> keys;

    public static Map<String, List<ItemCategoria>> getMapItensPorCategoria() {
        if(mapItensPorCategoria == null){
            mapItensPorCategoria = new HashMap<String, List<ItemCategoria>>();
        }

        return mapItensPorCategoria;
    }

    public static void setMapItensPorCategoria(Map<String, List<ItemCategoria>> mapItensPorCategoria) {
        FavoriteApp.mapItensPorCategoria = mapItensPorCategoria;
    }

    public static Map<String, List<Bitmap>> getMapImagensPorItem() {
        if(mapImagensPorItem == null){
            mapImagensPorItem = new HashMap<String, List<Bitmap>>();
        }
        return mapImagensPorItem;
    }

    public static void setMapImagensPorItem(Map<String, List<Bitmap>> mapImagensPorItem) {
        FavoriteApp.mapImagensPorItem = mapImagensPorItem;
    }

    public static SharedPreferences getPreferences(DrawerLayoutActivity context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static List<String> getKeys() {
        if(keys == null){
            keys = new ArrayList<String>();
        }

        return keys;
    }
}
