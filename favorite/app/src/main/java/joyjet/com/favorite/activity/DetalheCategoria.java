package joyjet.com.favorite.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import joyjet.com.favorite.R;
import joyjet.com.favorite.adapter.GaleriaAdapter;
import joyjet.com.favorite.app.FavoriteApp;
import joyjet.com.favorite.model.ItemCategoria;

public class DetalheCategoria extends DrawerLayoutActivity {

    String categoria;
    int indiceItem;
    TextView textViewTituloItemDetalhe, textViewTituloCategoriaDetalhe, textViewDescricaoItemDetalhe;
    ViewPager viewPagerGaleriaDetalhes;
    Map<String, List<Bitmap>> mapImagensPorItem;
    LinearLayout linearLayoutVoltar, linearLayoutFavorito;
    ItemCategoria itemCategoria;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_detalhe_categoria);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        toolbar.setVisibility(View.GONE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_menu));

        View view = LayoutInflater.from(this).inflate(R.layout.activity_detalhe_categoria, null);

        categoria = getIntent().getStringExtra("categoria");
        indiceItem = getIntent().getIntExtra("itemIndice", 0);
        mapImagensPorItem = FavoriteApp.getMapImagensPorItem();

        textViewTituloCategoriaDetalhe = (TextView) view.findViewById(R.id.text_view_titulo_categoria_detalhe);
        textViewTituloItemDetalhe = (TextView) view.findViewById(R.id.text_view_titulo_item_detalhe);
        textViewDescricaoItemDetalhe = (TextView) view.findViewById(R.id.text_view_descricao_item_detalhe);

        viewPagerGaleriaDetalhes = (ViewPager) view.findViewById(R.id.view_pager_galeria_detalhes);

        linearLayoutVoltar = (LinearLayout) view.findViewById(R.id.linear_layout_voltar);
        linearLayoutFavorito = (LinearLayout) view.findViewById(R.id.linear_layout_favorito);

        if(mapImagensPorItem.get(categoria + "_" + indiceItem) != null){
            GaleriaAdapter galeriaAdapter = new GaleriaAdapter(DetalheCategoria.this, mapImagensPorItem.get(categoria + "_" + indiceItem), categoria, indiceItem, false, false);
            viewPagerGaleriaDetalhes.setAdapter(galeriaAdapter);
            galeriaAdapter.notifyDataSetChanged();
        }else{
            Toast.makeText(DetalheCategoria.this, "Não foi possivel carregar as imagens", Toast.LENGTH_LONG).show();
        }

        contentFrame.addView(view);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(FavoriteApp.getMapItensPorCategoria().get(categoria) != null){
            itemCategoria = FavoriteApp.getMapItensPorCategoria().get(categoria).get(indiceItem);
        }else{
            itemCategoria = new ItemCategoria();
        }

        textViewTituloCategoriaDetalhe.setText(categoria);
        textViewTituloItemDetalhe.setText(itemCategoria.getTitulo());
        textViewDescricaoItemDetalhe.setText(itemCategoria.getDescricao());

        linearLayoutVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        linearLayoutFavorito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPref = FavoriteApp.getPreferences(DetalheCategoria.this);//getSharedPreferences("favoritos", Context.MODE_PRIVATE);
                Set<String> favoritos = sharedPref.getStringSet("favoritosList", null);
                SharedPreferences.Editor editor = sharedPref.edit();
                if(favoritos == null){
                    favoritos = new HashSet<String>();
                }

                if(!favoritos.contains(categoria + "_" + indiceItem)) {
                    favoritos.add(categoria + "_" + indiceItem);
                    editor.putStringSet("favoritosList", favoritos);
                    editor.apply();
                    Toast.makeText(DetalheCategoria.this, "Favorito salvo com sucesso", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(DetalheCategoria.this, "Item já adicionado aos favoritos", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
