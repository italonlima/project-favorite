package joyjet.com.favorite.widget;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;

public class ActionBarDrawableToogle extends ActionBarDrawerToggle {

    private ActionBarActivity hostActivity;
    private int openedResource;
    private int closedResource;

    public ActionBarDrawableToogle(ActionBarActivity activity, DrawerLayout drawerLayout, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
        super(activity, drawerLayout, openDrawerContentDescRes, closeDrawerContentDescRes);

        hostActivity = activity;
        openedResource = openDrawerContentDescRes;
        closedResource = closeDrawerContentDescRes;
    }
}
