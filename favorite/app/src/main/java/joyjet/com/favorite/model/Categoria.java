package joyjet.com.favorite.model;


import java.util.List;

public class Categoria {
    private String nome;
    private List<ItemCategoria> itensCategoria;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ItemCategoria> getItensCategoria() {
        return itensCategoria;
    }

    public void setItensCategoria(List<ItemCategoria> itensCategoria) {
        this.itensCategoria = itensCategoria;
    }
}
