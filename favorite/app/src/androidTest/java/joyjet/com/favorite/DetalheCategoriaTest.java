package joyjet.com.favorite;

import android.support.test.annotation.UiThreadTest;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.LinearLayout;

import joyjet.com.favorite.activity.DetalheCategoria;

public class DetalheCategoriaTest extends ActivityInstrumentationTestCase2<DetalheCategoria>{

    private DetalheCategoria activity;
    private LinearLayout linearLayoutVoltar, linearLayoutFavorito;

    public DetalheCategoriaTest() {
        super(DetalheCategoria.class);
    }

    // Inicializa os componentes que serão testados
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        activity = getActivity();
        linearLayoutVoltar = (LinearLayout) activity.findViewById(R.id.linear_layout_voltar);
        linearLayoutFavorito = (LinearLayout) activity.findViewById(R.id.linear_layout_favorito);
    }

    // Boa prática, verifica se os componentes foram incializados corretamente antes de continuar
    public void testPreconditions() {
        assertNotNull("DetalheCategoria is null", activity);
        assertNotNull("button voltar is null", linearLayoutVoltar);
        assertNotNull("button favorite is null", linearLayoutFavorito);
    }
}
